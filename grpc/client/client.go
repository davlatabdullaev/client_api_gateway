package client

import (
	"client_api_gateway/config"
	pbu "client_api_gateway/genproto/user_service"
	"google.golang.org/grpc"
)

type IServiceManger interface {
	CustomerService() pbu.CustomerServiceClient
	HotelService() pbu.HotelServiceClient
}

type grpcClients struct {
	customerService pbu.CustomerServiceClient
	hotelService    pbu.HotelServiceClient
}

func NewGrpcClients(cfg config.Config) (IServiceManger, error) {
	connUserService, err := grpc.Dial(
		cfg.UserGrpcServiceHost+cfg.UserGrpcServicePort,
		grpc.WithInsecure(),
	)
	if err != nil {
		return nil, err
	}

	return &grpcClients{
		customerService: pbu.NewCustomerServiceClient(connUserService),
		hotelService:    pbu.NewHotelServiceClient(connUserService),
	}, nil
}

func (g *grpcClients) CustomerService() pbu.CustomerServiceClient {
	return g.customerService
}

func (g *grpcClients) HotelService() pbu.HotelServiceClient {
	return g.hotelService
}
