#!/bin/zsh

CURRENT_DIR=$(pwd)

for x in $(find ${CURRENT_DIR}/trip_protos/* -type d); do
  sudo protoc -I=${x} -I=${CURRENT_DIR}/trip_protos -I /usr/local/include --go_out=${CURRENT_DIR} --go-grpc_out=${CURRENT_DIR} ${x}/*.proto
done

