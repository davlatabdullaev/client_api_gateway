package main

import (
	"client_api_gateway/api"
	"client_api_gateway/api/handler"
	"client_api_gateway/config"
	"client_api_gateway/grpc/client"
	"client_api_gateway/pkg/logger"
	"github.com/gin-gonic/gin"
)

func main() {
	cfg := config.Load()

	loggerLevel := logger.LevelDebug

	switch cfg.Environment {
	case config.DebugMode:
		loggerLevel = logger.LevelDebug
		gin.SetMode(gin.DebugMode)
	case config.TestMode:
		loggerLevel = logger.LevelDebug
		gin.SetMode(gin.TestMode)
	default:
		loggerLevel = logger.LevelInfo
		gin.SetMode(gin.ReleaseMode)
	}

	log := logger.NewLogger(cfg.ServiceName, loggerLevel)
	defer logger.Cleanup(log)

	services, err := client.NewGrpcClients(cfg)
	if err != nil {
		log.Error("Error while initializing grpc clients", logger.Error(err))
		return
	}

	h := handler.New(cfg, log, services)

	r := api.New(h)

	log.Info("Server is running ...", logger.Any("port", cfg.HTTPPort))
	if err = r.Run(cfg.HTTPPort); err != nil {
		log.Error("Error while running server", logger.Error(err))
	}
}
