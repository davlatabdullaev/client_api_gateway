CURRENT_DIR=$(shell pwd)

run:
	go run cmd/main.go

swag-init:
	swag init -g api/router.go -o api/docs

proto-gen:
	./scripts/gen_proto.sh
