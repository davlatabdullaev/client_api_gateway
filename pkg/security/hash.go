package security

import "golang.org/x/crypto/bcrypt"

const (
	// A2IDTime is the number of iterations (or passes) over the memory used by the algorithm Argon2id
	A2IDTime = 3

	// A2IDMemory is the amount of memory used by the algorithm Argon2id
	A2IDMemory = 64 * 1024

	// A2IDThreads is the number of threads used by the algorithm Argon2id
	A2IDThreads = 4

	// A2IDKeyLen is the length of the generated key (or password hash) by the algorithm Argon2id. 16 bytes or more is recommended.
	A2IDKeyLen = 32

	// A2IDSaltLen is the length of the random salt used by the algorithm Argon2id. 16 bytes is recommended for password hashing.
	A2IDSaltLen = 16
)

func HashPassword(password string) (string, error) {
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		return "", err
	}

	return string(hashedPassword), nil
}

func CompareHashAndPassword(hashedPassword, password string) bool {
	return bcrypt.CompareHashAndPassword([]byte(hashedPassword), []byte(password)) == nil
}

/*
// HashPassword is used to generate a new password hash for storing and comparing at a later date.
func HashPassword(password string) (hashedPassword string, err error) {

	// Generate a cryptographically secure random salt.
	salt, err := GenerateRandomBytes(A2IDSaltLen)
	if err != nil {
		return "", err
	}

	hash := argon2.IDKey([]byte(password), salt, A2IDTime, A2IDMemory, A2IDThreads, A2IDKeyLen)

	// Base64 encode the salt and hashed password.
	b64Salt := base64.RawStdEncoding.EncodeToString(salt)
	b64Hash := base64.RawStdEncoding.EncodeToString(hash)

	format := "$argon2id$v=%d$models=%d,t=%d,p=%d$%s$%s"
	hashedPassword = fmt.Sprintf(format, argon2.Version, A2IDMemory, A2IDTime, A2IDThreads, b64Salt, b64Hash)
	return hashedPassword, nil
}

// ComparePassword is used to compare a user-inputted password to a hash to see if the password matches or not.
func ComparePassword(hashedPassword, password string) (match bool, err error) {
	parts := strings.Split(hashedPassword, "$")

	if len(parts) <= 5 {
		return false, errors.New("incorrect hashed")
	}

	memory := A2IDMemory
	time := A2IDTime
	threads := A2IDThreads

	_, err = fmt.Sscanf(parts[3], "models=%d,t=%d,p=%d", &memory, &time, &threads)
	if err != nil {
		return false, err
	}

	salt, err := base64.RawStdEncoding.DecodeString(parts[4])
	if err != nil {
		return false, err
	}

	decodedHash, err := base64.RawStdEncoding.DecodeString(parts[5])
	if err != nil {
		return false, err
	}
	keyLen := uint32(len(decodedHash))

	comparisonHash := argon2.IDKey([]byte(password), salt, A2IDTime, A2IDMemory, A2IDThreads, keyLen)

	return subtle.ConstantTimeCompare(decodedHash, comparisonHash) == 1, nil
}
*/
