package api

import (
	"client_api_gateway/api/handler"
	"github.com/gin-gonic/gin"

	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"

	_ "client_api_gateway/api/docs"
)

// New ...
// @title           Swagger Example API
// @version         1.0
// @description     This is a sample server celler server.
// @securityDefinitions.apikey ApiKeyAuth
// @in header
// @name Authorization
func New(h handler.Handler) *gin.Engine {
	r := gin.New()

	r.POST("/customer", h.CreateCustomer)
	r.POST("/hotel", h.CreateHotel)

	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

	return r
}
