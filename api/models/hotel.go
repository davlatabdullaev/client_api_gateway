package models

type CreateHotel struct {
	Name        Language `json:"name"`
	Phone       string   `json:"phone"`
	Description Language `json:"description"`
	Address     string   `json:"address"`
	Lat         float32  `json:"lat"`
	Long        float32  `json:"long"`
	CityID      string   `json:"city_id"`
}

type Hotel struct {
	ID          string   `json:"id"`
	Name        Language `json:"name"`
	Phone       string   `json:"phone"`
	Description Language `json:"description"`
	Address     string   `json:"address"`
	Lat         float32  `json:"lat"`
	Long        float32  `json:"long"`
	CityID      string   `json:"city_id"`
}
