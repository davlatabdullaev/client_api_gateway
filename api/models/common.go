package models

type Language struct {
	Ru string `json:"ru"`
	En string `json:"en"`
}
