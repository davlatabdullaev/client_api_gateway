package handler

import (
	pbu "client_api_gateway/genproto/user_service"
	"context"
	"github.com/gin-gonic/gin"
	"net/http"
	"time"
)

// CreateHotel godoc
// @Router       /hotel [POST]
// @Summary      Create a new hotel
// @Description  Create a new hotel
// @Tags         hotel
// @Accept       json
// @Produce      json
// @Param        hotel body models.CreateHotel false "hotel"
// @Success      201  {object}  models.Hotel
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) CreateHotel(c *gin.Context) {
	request := pbu.CreateHotelRequest{}

	if err := c.ShouldBindJSON(&request); err != nil {
		handleResponse(c, h.log, "error while reading body from client", http.StatusBadRequest, err)
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	resp, err := h.services.HotelService().Create(ctx, &request)
	if err != nil {
		handleResponse(c, h.log, "error while creating customer", http.StatusInternalServerError, err)
		return
	}

	handleResponse(c, h.log, "customer created", http.StatusCreated, resp)
}
