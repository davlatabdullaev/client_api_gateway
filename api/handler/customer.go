package handler

import (
	pbu "client_api_gateway/genproto/user_service"
	"context"
	"github.com/gin-gonic/gin"
	"net/http"
	"time"
)

// CreateCustomer godoc
// @Router       /customer [POST]
// @Summary      Create a new customer
// @Description  Create a new customer
// @Tags         customer
// @Accept       json
// @Produce      json
// @Param        customer body models.CreateCustomer false "customer"
// @Success      201  {object}  models.Customer
// @Failure      400  {object}  models.Response
// @Failure      404  {object}  models.Response
// @Failure      500  {object}  models.Response
func (h Handler) CreateCustomer(c *gin.Context) {
	request := pbu.CreateCustomerRequest{}

	if err := c.ShouldBindJSON(&request); err != nil {
		handleResponse(c, h.log, "error while reading body from client", http.StatusBadRequest, err)
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	resp, err := h.services.CustomerService().Create(ctx, &request)
	if err != nil {
		handleResponse(c, h.log, "error while creating customer", http.StatusInternalServerError, err)
		return
	}

	handleResponse(c, h.log, "customer created", http.StatusCreated, resp)
}
